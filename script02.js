let userNumber = prompt("Please, enter your number here (integer, greater then 1):");
while(Boolean(userNumber) == false) {
    userNumber = prompt("Incorrect! Please, enter correct number (integer, greater then 1):", [userNumber]);
}
while((userNumber % 1) !=0) {
    userNumber = prompt("Incorrect! Please, enter correct number (integer, greater then 1):", [userNumber]);
}
while((userNumber > 2) == false) {
    userNumber = prompt("Incorrect! Please, enter correct number (integer, greater then 1):", [userNumber]);
}
while((userNumber < 3000) == false) {
    userNumber = prompt("Ну уж нет - не более 3000! Please, enter correct number (integer, greater then 1):", [userNumber]);
}

document.write("2<br>")

let i = 3;
let j = 2;


// Этот подход показывает, что я был на привальном пути, но смог выводить инверсионно - числа, которые не являются простыми. Как сделать наоборот, не задействуя функции и прочее? 

// for(i = 3; i <= userNumber; i++) {
//     for(j = 2; j < (i/2+1); j++) {
//         if(i % j != 0) {
//            continue;
//         } else {
//             document.write("not a prime number: " + i + "<br>");
//             break;
//         } 
//     } 
// } 


// этот подход я подсмотрел в интернете, но не понял до конца, что такое метка - к теме циклов она не встречалась!
primeNumbersSelect:
    for(i = 3; i <= userNumber; i++) {
        for(j = 2; j < (i/2+1); j++) {
            if(i % j == 0) continue primeNumbersSelect;
            }  
        document.write(i + "<br>");
    } 


